-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 28, 2019 at 09:40 AM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lms-laravel`
--

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `price` decimal(15,2) DEFAULT NULL,
  `course_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `published` tinyint(4) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `title`, `slug`, `description`, `price`, `course_image`, `start_date`, `published`, `created_at`, `updated_at`, `deleted_at`) VALUES
(6, 'Test', 'test-course', 'This is the course for testing.', NULL, '1561646390-wpid-mad_minion-wallpaper-10456139.jpg', '2019-06-28', 1, '2019-06-27 12:39:51', '2019-06-27 12:39:51', NULL),
(7, 'Html Crush Course', 'html-crush-course', '<p><b><u>HTML Crush course:</u>&nbsp; </b>HTML is a popular required markup language for building any website. This course will take you from zero to hero in HTML (Hyper Text Markup Language).</p><p><b>By the end of this course, You will be able:</b></p><ol><li>Build anything by html,</li><li>Understanding basic <b>CSS </b>and its function,</li><li>&nbsp;Build a fully responsive web page.</li></ol>', NULL, '1561704976-download.png', '2019-07-01', 1, '2019-06-28 04:56:17', '2019-06-28 05:00:28', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `course_student`
--

CREATE TABLE `course_student` (
  `course_id` int(10) UNSIGNED DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `rating` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `course_student`
--

INSERT INTO `course_student` (`course_id`, `user_id`, `rating`, `created_at`, `updated_at`) VALUES
(6, 3, 5, '2019-06-27 13:41:46', '2019-06-27 15:29:28'),
(7, 3, 5, '2019-06-28 05:32:16', '2019-06-28 05:32:29');

-- --------------------------------------------------------

--
-- Table structure for table `course_user`
--

CREATE TABLE `course_user` (
  `course_id` int(10) UNSIGNED DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `course_user`
--

INSERT INTO `course_user` (`course_id`, `user_id`) VALUES
(6, 2),
(7, 2);

-- --------------------------------------------------------

--
-- Table structure for table `lessons`
--

CREATE TABLE `lessons` (
  `id` int(10) UNSIGNED NOT NULL,
  `course_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lesson_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `short_text` text COLLATE utf8mb4_unicode_ci,
  `full_text` text COLLATE utf8mb4_unicode_ci,
  `position` int(10) UNSIGNED DEFAULT NULL,
  `free_lesson` tinyint(4) DEFAULT '0',
  `published` tinyint(4) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `lessons`
--

INSERT INTO `lessons` (`id`, `course_id`, `title`, `slug`, `lesson_image`, `short_text`, `full_text`, `position`, `free_lesson`, `published`, `created_at`, `updated_at`, `deleted_at`) VALUES
(51, 6, 'Test Lesson', 'test-lesson', NULL, 'I\'m hell excited to have you in this course.', 'This is the awesome lesson. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\n	tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\n	quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\n	consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\n	cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\n	proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1, 1, 1, '2019-06-27 12:41:53', '2019-06-27 12:41:53', NULL),
(52, 7, 'HTML Elements', 'html-elements', NULL, '<p>In this lesson you will get basic HTML elements. In need for more, find in&nbsp;<a href=\"https://www.w3schools.com/html/html_elements.asp\" target=\"_blank\">HTML Elements</a>.</p>', '<p style=\"box-sizing: inherit; font-family: Verdana, sans-serif; font-size: 15px;\"><span style=\"font-family: &quot;Times New Roman&quot;;\">An HTML element usually consists of a&nbsp;</span><span style=\"box-sizing: inherit; font-weight: bolder; font-family: &quot;Times New Roman&quot;;\">start</span><span style=\"font-family: &quot;Times New Roman&quot;;\">&nbsp;</span><span style=\"font-family: &quot;Times New Roman&quot;;\">tag and an</span><span style=\"font-family: &quot;Times New Roman&quot;;\">&nbsp;</span><span style=\"box-sizing: inherit; font-weight: bolder; font-family: &quot;Times New Roman&quot;;\">end</span><span style=\"font-family: &quot;Times New Roman&quot;;\">&nbsp;</span><span style=\"font-family: &quot;Times New Roman&quot;;\">tag, with the content inserted in between:</span></p><div class=\"htmlHigh\" style=\"box-sizing: inherit; font-family: Verdana, sans-serif; font-size: 20px; padding: 10px; margin-bottom: 10px;\"><span class=\"tagnamecolor\" style=\"box-sizing: inherit; color: brown;\"><span class=\"tagcolor\" style=\"box-sizing: inherit; color: mediumblue; font-family: &quot;Times New Roman&quot;;\">&lt;</span><span style=\"font-family: &quot;Times New Roman&quot;;\">tagname</span><span class=\"tagcolor\" style=\"box-sizing: inherit; color: mediumblue; font-family: &quot;Times New Roman&quot;;\">&gt;</span></span><span style=\"font-family: &quot;Times New Roman&quot;;\">Content goes here...</span><span class=\"tagnamecolor\" style=\"box-sizing: inherit; color: brown;\"><span class=\"tagcolor\" style=\"box-sizing: inherit; color: mediumblue; font-family: &quot;Times New Roman&quot;;\">&lt;</span><span style=\"font-family: &quot;Times New Roman&quot;;\">/tagname</span><span class=\"tagcolor\" style=\"box-sizing: inherit; color: mediumblue; font-family: &quot;Times New Roman&quot;;\">&gt;</span></span></div><p style=\"box-sizing: inherit; font-family: Verdana, sans-serif; font-size: 15px;\"><span style=\"font-family: &quot;Times New Roman&quot;;\">The HTML</span><span style=\"font-family: &quot;Times New Roman&quot;;\">&nbsp;</span><span style=\"box-sizing: inherit; font-weight: bolder; font-family: &quot;Times New Roman&quot;;\">element</span><span style=\"font-family: &quot;Times New Roman&quot;;\">&nbsp;</span><span style=\"font-family: &quot;Times New Roman&quot;;\">is everything from the start tag to the end tag:</span></p><div class=\"htmlHigh\" style=\"box-sizing: inherit; font-family: Verdana, sans-serif; font-size: 20px; padding: 10px; margin-bottom: 10px;\"><span class=\"tagnamecolor\" style=\"box-sizing: inherit; color: brown;\"><span class=\"tagcolor\" style=\"box-sizing: inherit; color: mediumblue; font-family: &quot;Times New Roman&quot;;\">&lt;</span><span style=\"font-family: &quot;Times New Roman&quot;;\">p</span><span class=\"tagcolor\" style=\"box-sizing: inherit; color: mediumblue; font-family: &quot;Times New Roman&quot;;\">&gt;</span></span><span style=\"font-family: &quot;Times New Roman&quot;;\">My first paragraph.</span><span class=\"tagnamecolor\" style=\"box-sizing: inherit; color: brown;\"><span class=\"tagcolor\" style=\"box-sizing: inherit; color: mediumblue; font-family: &quot;Times New Roman&quot;;\">&lt;</span><span style=\"font-family: &quot;Times New Roman&quot;;\">/p</span><span class=\"tagcolor\" style=\"box-sizing: inherit; color: mediumblue; font-family: &quot;Times New Roman&quot;;\">&gt;</span></span></div><div class=\"htmlHigh\" style=\"box-sizing: inherit; font-family: Verdana, sans-serif; font-size: 20px; padding: 10px; margin-bottom: 10px;\"><span class=\"tagnamecolor\" style=\"box-sizing: inherit; color: brown;\"><span class=\"tagcolor\" style=\"box-sizing: inherit; color: mediumblue; font-family: &quot;Times New Roman&quot;;\"><br></span></span></div><table class=\"table table-bordered\"><tbody><tr><td><span style=\"font-family: &quot;Times New Roman&quot;; font-size: 15px; font-weight: 700;\">Start tag</span><b><br></b></td><td><b>Element Content</b></td><td><b>End Tag</b></td></tr><tr><td>&lt;h1&gt;</td><td>My First Heading</td><td>&lt;/h1&gt;</td></tr><tr><td>&lt;p&gt;</td><td>My first paragraph</td><td>&lt;/p&gt;</td></tr><tr><td>&lt;br/&gt;</td><td><br></td><td><br></td></tr></tbody></table><div class=\"htmlHigh\" style=\"box-sizing: inherit; font-family: Verdana, sans-serif; font-size: 20px; padding: 10px; margin-bottom: 10px;\"><span class=\"tagnamecolor\" style=\"box-sizing: inherit; color: brown;\"><span class=\"tagcolor\" style=\"box-sizing: inherit; color: mediumblue; font-family: &quot;Times New Roman&quot;;\"><span style=\"font-family: Verdana, sans-serif; font-size: 15px; background-color: rgb(255, 255, 204);\">HTML elements with no content are called empty elements. Empty elements do not have an end tag, such as the &lt;br&gt; element (which indicates a line break).</span><br></span></span></div><div class=\"htmlHigh\" style=\"box-sizing: inherit; font-family: Verdana, sans-serif; font-size: 20px; padding: 10px; margin-bottom: 10px;\"><span class=\"tagnamecolor\" style=\"box-sizing: inherit; color: brown;\"><span class=\"tagcolor\" style=\"box-sizing: inherit; color: mediumblue; font-family: &quot;Times New Roman&quot;;\"><br></span></span></div><div class=\"htmlHigh\" style=\"box-sizing: inherit; font-family: Verdana, sans-serif; font-size: 20px; padding: 10px; margin-bottom: 10px;\"><span class=\"tagnamecolor\" style=\"box-sizing: inherit; color: brown;\"><span class=\"tagcolor\" style=\"box-sizing: inherit; color: mediumblue;\"><br></span></span></div><div class=\"htmlHigh\" style=\"box-sizing: inherit; font-family: Verdana, sans-serif; font-size: 20px; padding: 10px; margin-bottom: 10px;\"><span class=\"tagnamecolor\" style=\"box-sizing: inherit; color: brown;\"><span class=\"tagcolor\" style=\"box-sizing: inherit; color: mediumblue;\"><br></span></span></div>', 1, 0, 1, '2019-06-28 05:08:18', '2019-06-28 05:08:18', NULL),
(53, 7, 'Html Atrributes', 'html-attributes', NULL, '<p><strong>Attributes provide additional information about HTML elements.</strong></p><p><strong></strong><br></p><p><strong>&nbsp;</strong>&nbsp;In this module you will learn everything about html attribute.<br></p>', '<h2><u>HTML Attributes</u></h2>\r\n\r\n<ul>\r\n	<li>All HTML elements can have&nbsp;<strong>attributes</strong></li>\r\n	<li>Attributes provide&nbsp;<strong>additional information</strong>&nbsp;about an element</li>\r\n	<li>Attributes are always specified in&nbsp;<strong>the start tag</strong></li>\r\n	<li>Attributes usually come in name/value pairs like:&nbsp;<strong>name=&quot;value&quot;</strong></li>\r\n</ul>\r\n\r\n<h2><u>The href Attribute</u></h2>\r\n\r\n<p>HTML links are defined with the&nbsp;<code>&lt;a&gt;</code>&nbsp;tag. The link address is specified in the&nbsp;<code>href</code>&nbsp;attribute:</p>\r\n\r\n<h2><u>The src Attribute</u></h2>\r\n\r\n<p>HTML images are defined with the&nbsp;<code>&lt;img&gt;</code>&nbsp;tag.</p>\r\n\r\n<p>The filename of the image source is specified in the&nbsp;<code>src</code>&nbsp;attribute:</p>\r\n\r\n<p><strong>For more about attributes you can get them on&nbsp;</strong><a href=\"https://www.w3schools.com/html/html_attributes.asp\" target=\"_blank\">Html Attributes</a></p>', 2, 0, 0, '2019-06-28 05:13:16', '2019-06-28 05:15:48', '2019-06-28 05:15:48'),
(54, 7, 'Html Atrributes', 'html-attributes', NULL, '<p><strong>Attributes provide additional information about HTML elements.</strong></p><p><strong></strong><br></p><p><strong>&nbsp;</strong>&nbsp;In this module you will learn everything about html attribute.<br></p>', '<h2><u>HTML Attributes</u></h2>\r\n\r\n<ul>\r\n	<li>All HTML elements can have&nbsp;<strong>attributes</strong></li>\r\n	<li>Attributes provide&nbsp;<strong>additional information</strong>&nbsp;about an element</li>\r\n	<li>Attributes are always specified in&nbsp;<strong>the start tag</strong></li>\r\n	<li>Attributes usually come in name/value pairs like:&nbsp;<strong>name=&quot;value&quot;</strong></li>\r\n</ul>\r\n\r\n<h2><u>The href Attribute</u></h2>\r\n\r\n<p>HTML links are defined with the&nbsp;<code>&lt;a&gt;</code>&nbsp;tag. The link address is specified in the&nbsp;<code>href</code>&nbsp;attribute:</p>\r\n\r\n<h2><u>The src Attribute</u></h2>\r\n\r\n<p>HTML images are defined with the&nbsp;<code>&lt;img&gt;</code>&nbsp;tag.</p>\r\n\r\n<p>The filename of the image source is specified in the&nbsp;<code>src</code>&nbsp;attribute:</p>\r\n\r\n<p><strong>For more about attributes you can get them on&nbsp;</strong><a href=\"https://www.w3schools.com/html/html_attributes.asp\" target=\"_blank\">Html Attributes</a></p>', 3, 0, 1, '2019-06-28 05:13:20', '2019-06-28 05:13:20', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `lesson_student`
--

CREATE TABLE `lesson_student` (
  `lesson_id` int(10) UNSIGNED DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `lesson_student`
--

INSERT INTO `lesson_student` (`lesson_id`, `user_id`, `created_at`, `updated_at`) VALUES
(51, 3, '2019-06-27 12:53:35', '2019-06-27 12:53:35'),
(52, 3, '2019-06-28 05:36:54', '2019-06-28 05:36:54'),
(54, 3, '2019-06-28 05:37:11', '2019-06-28 05:37:11');

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE `media` (
  `id` int(10) UNSIGNED NOT NULL,
  `model_id` int(10) UNSIGNED DEFAULT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `collection_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `disk` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` int(10) UNSIGNED NOT NULL,
  `manipulations` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `custom_properties` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_column` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `media`
--

INSERT INTO `media` (`id`, `model_id`, `model_type`, `collection_name`, `name`, `file_name`, `disk`, `size`, `manipulations`, `custom_properties`, `order_column`, `created_at`, `updated_at`) VALUES
(1, 51, 'App\\Lesson', 'downloadable_files', 'Vue Html To Paper - Vue mixin for html elements printing', 'Vue Html To Paper - Vue mixin for html elements printing.pdf', 'media', 52371, '[]', '[]', 1, '2019-06-27 12:41:43', '2019-06-27 12:41:53'),
(2, NULL, 'App\\Lesson', 'downloadable_files', 'chrome', 'chrome.exe', 'media', 1555952, '[]', '[]', 2, '2019-06-27 12:42:51', '2019-06-27 12:42:51'),
(3, 51, 'App\\Lesson', 'downloadable_files', 'Shop Management', 'Shop Management.pdf', 'media', 56047, '[]', '[]', 2, '2019-06-27 12:43:12', '2019-06-27 12:43:21'),
(4, 52, 'App\\Lesson', 'downloadable_files', 'Vue Html To Paper - Vue mixin for html elements printing', 'Vue Html To Paper - Vue mixin for html elements printing.pdf', 'media', 52371, '[]', '[]', 3, '2019-06-28 05:08:13', '2019-06-28 05:08:18');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_100000_create_password_resets_table', 1),
(2, '2017_07_19_082005_create_1500441605_permissions_table', 1),
(3, '2017_07_19_082006_create_1500441606_roles_table', 1),
(4, '2017_07_19_082009_create_1500441609_users_table', 1),
(5, '2017_07_19_082347_create_1500441827_courses_table', 1),
(6, '2017_07_19_082723_create_1500442043_lessons_table', 1),
(7, '2017_07_19_082724_create_media_table', 1),
(8, '2017_07_19_082929_create_1500442169_questions_table', 1),
(9, '2017_07_19_083047_create_1500442247_questions_options_table', 1),
(10, '2017_07_19_083236_create_1500442356_tests_table', 1),
(11, '2017_07_19_120427_create_596eec08307cd_permission_role_table', 1),
(12, '2017_07_19_120430_create_596eec0af366b_role_user_table', 1),
(13, '2017_07_19_120808_create_596eece522a6e_course_user_table', 1),
(14, '2017_07_19_121657_create_596eeef709839_question_test_table', 1),
(15, '2017_08_14_085956_create_course_students_table', 1),
(16, '2017_08_17_051131_create_tests_results_table', 1),
(17, '2017_08_17_051254_create_tests_results_answers_table', 1),
(18, '2017_08_18_054622_create_lesson_student_table', 1),
(19, '2017_08_18_060324_add_rating_to_course_student_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `title`, `created_at`, `updated_at`) VALUES
(1, 'user_management_access', '2019-06-27 11:52:39', '2019-06-27 11:52:39'),
(2, 'user_management_create', '2019-06-27 11:52:39', '2019-06-27 11:52:39'),
(3, 'user_management_edit', '2019-06-27 11:52:39', '2019-06-27 11:52:39'),
(4, 'user_management_view', '2019-06-27 11:52:39', '2019-06-27 11:52:39'),
(5, 'user_management_delete', '2019-06-27 11:52:39', '2019-06-27 11:52:39'),
(6, 'permission_access', '2019-06-27 11:52:39', '2019-06-27 11:52:39'),
(7, 'permission_create', '2019-06-27 11:52:39', '2019-06-27 11:52:39'),
(8, 'permission_edit', '2019-06-27 11:52:39', '2019-06-27 11:52:39'),
(9, 'permission_view', '2019-06-27 11:52:39', '2019-06-27 11:52:39'),
(10, 'permission_delete', '2019-06-27 11:52:39', '2019-06-27 11:52:39'),
(11, 'role_access', '2019-06-27 11:52:39', '2019-06-27 11:52:39'),
(12, 'role_create', '2019-06-27 11:52:39', '2019-06-27 11:52:39'),
(13, 'role_edit', '2019-06-27 11:52:39', '2019-06-27 11:52:39'),
(14, 'role_view', '2019-06-27 11:52:39', '2019-06-27 11:52:39'),
(15, 'role_delete', '2019-06-27 11:52:39', '2019-06-27 11:52:39'),
(16, 'user_access', '2019-06-27 11:52:39', '2019-06-27 11:52:39'),
(17, 'user_create', '2019-06-27 11:52:39', '2019-06-27 11:52:39'),
(18, 'user_edit', '2019-06-27 11:52:40', '2019-06-27 11:52:40'),
(19, 'user_view', '2019-06-27 11:52:40', '2019-06-27 11:52:40'),
(20, 'user_delete', '2019-06-27 11:52:40', '2019-06-27 11:52:40'),
(21, 'course_access', '2019-06-27 11:52:40', '2019-06-27 11:52:40'),
(22, 'course_create', '2019-06-27 11:52:40', '2019-06-27 11:52:40'),
(23, 'course_edit', '2019-06-27 11:52:40', '2019-06-27 11:52:40'),
(24, 'course_view', '2019-06-27 11:52:40', '2019-06-27 11:52:40'),
(25, 'course_delete', '2019-06-27 11:52:40', '2019-06-27 11:52:40'),
(26, 'lesson_access', '2019-06-27 11:52:40', '2019-06-27 11:52:40'),
(27, 'lesson_create', '2019-06-27 11:52:40', '2019-06-27 11:52:40'),
(28, 'lesson_edit', '2019-06-27 11:52:40', '2019-06-27 11:52:40'),
(29, 'lesson_view', '2019-06-27 11:52:40', '2019-06-27 11:52:40'),
(30, 'lesson_delete', '2019-06-27 11:52:40', '2019-06-27 11:52:40'),
(31, 'question_access', '2019-06-27 11:52:40', '2019-06-27 11:52:40'),
(32, 'question_create', '2019-06-27 11:52:40', '2019-06-27 11:52:40'),
(33, 'question_edit', '2019-06-27 11:52:40', '2019-06-27 11:52:40'),
(34, 'question_view', '2019-06-27 11:52:40', '2019-06-27 11:52:40'),
(35, 'question_delete', '2019-06-27 11:52:40', '2019-06-27 11:52:40'),
(36, 'questions_option_access', '2019-06-27 11:52:40', '2019-06-27 11:52:40'),
(37, 'questions_option_create', '2019-06-27 11:52:40', '2019-06-27 11:52:40'),
(38, 'questions_option_edit', '2019-06-27 11:52:40', '2019-06-27 11:52:40'),
(39, 'questions_option_view', '2019-06-27 11:52:41', '2019-06-27 11:52:41'),
(40, 'questions_option_delete', '2019-06-27 11:52:41', '2019-06-27 11:52:41'),
(41, 'test_access', '2019-06-27 11:52:41', '2019-06-27 11:52:41'),
(42, 'test_create', '2019-06-27 11:52:41', '2019-06-27 11:52:41'),
(43, 'test_edit', '2019-06-27 11:52:41', '2019-06-27 11:52:41'),
(44, 'test_view', '2019-06-27 11:52:41', '2019-06-27 11:52:41'),
(45, 'test_delete', '2019-06-27 11:52:41', '2019-06-27 11:52:41');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED DEFAULT NULL,
  `role_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(1, 2),
(21, 2),
(22, 2),
(23, 2),
(24, 2),
(26, 2),
(27, 2),
(28, 2),
(29, 2),
(31, 2),
(32, 2),
(33, 2),
(34, 2),
(36, 2),
(37, 2),
(38, 2),
(39, 2),
(40, 2),
(41, 2),
(42, 2),
(43, 2),
(44, 2),
(45, 2),
(1, 3),
(21, 3),
(24, 3),
(26, 3),
(29, 3),
(31, 3),
(34, 3),
(36, 3),
(37, 3),
(38, 3),
(39, 3),
(40, 3),
(41, 3),
(44, 3);

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(10) UNSIGNED NOT NULL,
  `question` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `question_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `score` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `question`, `question_image`, `score`, `created_at`, `updated_at`, `deleted_at`) VALUES
(51, 'What is the purpose of this course?', NULL, 5, '2019-06-27 12:49:38', '2019-06-27 12:49:38', NULL),
(52, 'What is a markup language?', NULL, 5, '2019-06-28 05:28:06', '2019-06-28 05:28:06', NULL),
(53, 'What does <p> tag does?', NULL, 5, '2019-06-28 05:29:43', '2019-06-28 05:29:43', NULL),
(54, 'What does href tag do?', NULL, 5, '2019-06-28 05:31:40', '2019-06-28 05:31:40', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `questions_options`
--

CREATE TABLE `questions_options` (
  `id` int(10) UNSIGNED NOT NULL,
  `question_id` int(10) UNSIGNED DEFAULT NULL,
  `option_text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `correct` tinyint(4) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `questions_options`
--

INSERT INTO `questions_options` (`id`, `question_id`, `option_text`, `correct`, `created_at`, `updated_at`, `deleted_at`) VALUES
(201, 51, 'Testing', 0, '2019-06-27 12:49:38', '2019-06-27 12:49:38', NULL),
(202, 51, 'Challenging stupids', 1, '2019-06-27 12:49:38', '2019-06-27 12:49:38', NULL),
(203, 51, 'Serous question', 0, '2019-06-27 12:49:39', '2019-06-27 12:49:39', NULL),
(204, 51, 'No one of above is true', 0, '2019-06-27 12:49:39', '2019-06-27 12:49:39', NULL),
(205, 52, 'Programming Language', 0, '2019-06-28 05:28:06', '2019-06-28 05:28:06', NULL),
(206, 52, 'Designing Language', 0, '2019-06-28 05:28:06', '2019-06-28 05:28:06', NULL),
(207, 52, 'Language which describes the structure of a Web page', 1, '2019-06-28 05:28:06', '2019-06-28 05:28:06', NULL),
(208, 52, 'None of these is correct.', 0, '2019-06-28 05:28:06', '2019-06-28 05:28:06', NULL),
(209, 53, 'Primary Tag', 0, '2019-06-28 05:29:43', '2019-06-28 05:29:43', NULL),
(210, 53, 'Paragraph Tag', 1, '2019-06-28 05:29:43', '2019-06-28 05:29:43', NULL),
(211, 53, 'Progress tag', 0, '2019-06-28 05:29:44', '2019-06-28 05:29:44', NULL),
(212, 53, 'None of above is true.', 0, '2019-06-28 05:29:44', '2019-06-28 05:29:44', NULL),
(213, 54, 'Defines Image Location', 0, '2019-06-28 05:31:40', '2019-06-28 05:31:40', NULL),
(214, 54, 'Defines Link Location', 1, '2019-06-28 05:31:40', '2019-06-28 05:31:40', NULL),
(215, 54, 'Define CSS stylesheet location', 1, '2019-06-28 05:31:40', '2019-06-28 05:31:40', NULL),
(216, 54, 'All above are true', 0, '2019-06-28 05:31:40', '2019-06-28 05:31:40', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `question_test`
--

CREATE TABLE `question_test` (
  `question_id` int(10) UNSIGNED DEFAULT NULL,
  `test_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `question_test`
--

INSERT INTO `question_test` (`question_id`, `test_id`) VALUES
(51, 52),
(52, 53),
(52, 54),
(53, 53),
(54, 54);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `title`, `created_at`, `updated_at`) VALUES
(1, 'Administrator (can create other users)', '2019-06-27 11:52:41', '2019-06-27 11:52:41'),
(2, 'Teacher', '2019-06-27 11:52:41', '2019-06-27 11:52:41'),
(3, 'Student', '2019-06-27 11:52:41', '2019-06-27 11:52:41');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `role_id` int(10) UNSIGNED DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`role_id`, `user_id`) VALUES
(1, 1),
(2, 2),
(3, 3);

-- --------------------------------------------------------

--
-- Table structure for table `tests`
--

CREATE TABLE `tests` (
  `id` int(10) UNSIGNED NOT NULL,
  `course_id` int(10) UNSIGNED DEFAULT NULL,
  `lesson_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `published` tinyint(4) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tests`
--

INSERT INTO `tests` (`id`, `course_id`, `lesson_id`, `title`, `description`, `published`, `created_at`, `updated_at`, `deleted_at`) VALUES
(52, 6, 51, 'General Test', 'This is the test at term beginning for testing student\'s stupidity.', 1, '2019-06-27 12:51:22', '2019-06-27 12:51:22', NULL),
(53, 7, 52, 'Html Elements Test', 'Test for testing if you had understand HTML element is.', 1, '2019-06-28 05:24:30', '2019-06-28 05:24:30', NULL),
(54, 7, 54, 'Html Attributes Test', 'Testing if you have well learned this crucial lesson.', 1, '2019-06-28 05:25:25', '2019-06-28 05:25:25', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tests_results`
--

CREATE TABLE `tests_results` (
  `id` int(10) UNSIGNED NOT NULL,
  `test_id` int(10) UNSIGNED DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `test_result` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tests_results`
--

INSERT INTO `tests_results` (`id`, `test_id`, `user_id`, `test_result`, `created_at`, `updated_at`) VALUES
(1, 52, NULL, 0, '2019-06-27 12:52:19', '2019-06-27 12:52:19'),
(2, 52, 3, 5, '2019-06-27 12:53:42', '2019-06-27 12:53:42'),
(3, 54, 3, 5, '2019-06-28 05:38:18', '2019-06-28 05:38:18');

-- --------------------------------------------------------

--
-- Table structure for table `tests_results_answers`
--

CREATE TABLE `tests_results_answers` (
  `id` int(10) UNSIGNED NOT NULL,
  `tests_result_id` int(10) UNSIGNED DEFAULT NULL,
  `question_id` int(10) UNSIGNED DEFAULT NULL,
  `option_id` int(10) UNSIGNED DEFAULT NULL,
  `correct` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tests_results_answers`
--

INSERT INTO `tests_results_answers` (`id`, `tests_result_id`, `question_id`, `option_id`, `correct`, `created_at`, `updated_at`) VALUES
(1, 1, 51, 204, 0, '2019-06-27 12:52:19', '2019-06-27 12:52:19'),
(2, 2, 51, 202, 1, '2019-06-27 12:53:42', '2019-06-27 12:53:42'),
(3, 3, 52, 205, 0, '2019-06-28 05:38:18', '2019-06-28 05:38:18'),
(4, 3, 54, 215, 1, '2019-06-28 05:38:18', '2019-06-28 05:38:18');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@admin.com', '$2y$10$l4MghrLnKXTRUDlR07XQeesKHRIaAe7WzDf90g751BEf70AwnJ5m.', 'gqi8VuGGH4sHtNhfsGFnNiE576Jm515aewQGoPphgVJbGEI2VkNUoZVqlkAU', '2019-06-27 11:52:41', '2019-06-27 11:52:41'),
(2, 'Alain MUCYO', 'teacher@teacher.com', '$2y$10$pFLjRgHxAkNIeNIFe2x4k.bL0vwGHaof3Kj4/Ifwitsc6HpV4NoMm', 'VkQ6tfYBS7nvsQgcpo00OtcAqtBXFItZ44NfZULduDEQXnF6fwlgg4thvsHc', '2019-06-27 12:01:07', '2019-06-27 23:36:27'),
(3, 'Alain MUCYO', 'student@student.com', '$2y$10$yMjXN4xyvQeWG5ZOkAGyJ.jqQzUbeHiftfuPbiF8f8.KebGA3bCb6', 'eSDlfsQRXIIrh3eWjgwue36DVuzzbgw5uuQDflMlwjWCCFhSkKcyDs5re4oz', '2019-06-27 12:04:24', '2019-06-27 23:36:52');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `courses_deleted_at_index` (`deleted_at`);

--
-- Indexes for table `course_student`
--
ALTER TABLE `course_student`
  ADD KEY `course_student_course_id_foreign` (`course_id`),
  ADD KEY `course_student_user_id_foreign` (`user_id`);

--
-- Indexes for table `course_user`
--
ALTER TABLE `course_user`
  ADD KEY `fk_p_54418_54417_user_cou_596eece522b73` (`course_id`),
  ADD KEY `fk_p_54417_54418_course_u_596eece522bee` (`user_id`);

--
-- Indexes for table `lessons`
--
ALTER TABLE `lessons`
  ADD PRIMARY KEY (`id`),
  ADD KEY `54419_596eedbb6686e` (`course_id`),
  ADD KEY `lessons_deleted_at_index` (`deleted_at`);

--
-- Indexes for table `lesson_student`
--
ALTER TABLE `lesson_student`
  ADD KEY `lesson_student_lesson_id_foreign` (`lesson_id`),
  ADD KEY `lesson_student_user_id_foreign` (`user_id`);

--
-- Indexes for table `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`id`),
  ADD KEY `media_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD KEY `fk_p_54415_54416_role_per_596eec08308d0` (`permission_id`),
  ADD KEY `fk_p_54416_54415_permissi_596eec0830986` (`role_id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `questions_deleted_at_index` (`deleted_at`);

--
-- Indexes for table `questions_options`
--
ALTER TABLE `questions_options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `54421_596eee8745a1e` (`question_id`),
  ADD KEY `questions_options_deleted_at_index` (`deleted_at`);

--
-- Indexes for table `question_test`
--
ALTER TABLE `question_test`
  ADD KEY `fk_p_54420_54422_test_que_596eeef70992f` (`question_id`),
  ADD KEY `fk_p_54422_54420_question_596eeef7099af` (`test_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD KEY `fk_p_54416_54417_user_rol_596eec0af3746` (`role_id`),
  ADD KEY `fk_p_54417_54416_role_use_596eec0af37c1` (`user_id`);

--
-- Indexes for table `tests`
--
ALTER TABLE `tests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `54422_596eeef514d00` (`course_id`),
  ADD KEY `54422_596eeef53411a` (`lesson_id`),
  ADD KEY `tests_deleted_at_index` (`deleted_at`);

--
-- Indexes for table `tests_results`
--
ALTER TABLE `tests_results`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tests_results_test_id_foreign` (`test_id`),
  ADD KEY `tests_results_user_id_foreign` (`user_id`);

--
-- Indexes for table `tests_results_answers`
--
ALTER TABLE `tests_results_answers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tests_results_answers_tests_result_id_foreign` (`tests_result_id`),
  ADD KEY `tests_results_answers_question_id_foreign` (`question_id`),
  ADD KEY `tests_results_answers_option_id_foreign` (`option_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `lessons`
--
ALTER TABLE `lessons`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `media`
--
ALTER TABLE `media`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `questions_options`
--
ALTER TABLE `questions_options`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=217;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tests`
--
ALTER TABLE `tests`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `tests_results`
--
ALTER TABLE `tests_results`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tests_results_answers`
--
ALTER TABLE `tests_results_answers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `course_student`
--
ALTER TABLE `course_student`
  ADD CONSTRAINT `course_student_course_id_foreign` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `course_student_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `course_user`
--
ALTER TABLE `course_user`
  ADD CONSTRAINT `fk_p_54417_54418_course_u_596eece522bee` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_p_54418_54417_user_cou_596eece522b73` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `lessons`
--
ALTER TABLE `lessons`
  ADD CONSTRAINT `54419_596eedbb6686e` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `lesson_student`
--
ALTER TABLE `lesson_student`
  ADD CONSTRAINT `lesson_student_lesson_id_foreign` FOREIGN KEY (`lesson_id`) REFERENCES `lessons` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `lesson_student_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `fk_p_54415_54416_role_per_596eec08308d0` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_p_54416_54415_permissi_596eec0830986` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `questions_options`
--
ALTER TABLE `questions_options`
  ADD CONSTRAINT `54421_596eee8745a1e` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `question_test`
--
ALTER TABLE `question_test`
  ADD CONSTRAINT `fk_p_54420_54422_test_que_596eeef70992f` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_p_54422_54420_question_596eeef7099af` FOREIGN KEY (`test_id`) REFERENCES `tests` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `fk_p_54416_54417_user_rol_596eec0af3746` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_p_54417_54416_role_use_596eec0af37c1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `tests`
--
ALTER TABLE `tests`
  ADD CONSTRAINT `54422_596eeef514d00` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `54422_596eeef53411a` FOREIGN KEY (`lesson_id`) REFERENCES `lessons` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `tests_results`
--
ALTER TABLE `tests_results`
  ADD CONSTRAINT `tests_results_test_id_foreign` FOREIGN KEY (`test_id`) REFERENCES `tests` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tests_results_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `tests_results_answers`
--
ALTER TABLE `tests_results_answers`
  ADD CONSTRAINT `tests_results_answers_option_id_foreign` FOREIGN KEY (`option_id`) REFERENCES `questions_options` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tests_results_answers_question_id_foreign` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tests_results_answers_tests_result_id_foreign` FOREIGN KEY (`tests_result_id`) REFERENCES `tests_results` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
