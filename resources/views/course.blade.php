@extends('layouts.home')

@section('main')
    <div class="page-header">
        <h3>{{ $course->title }}</h3>
    </div>
    <div class="col-md-8">
        <p>{!! $course->description !!}</p>
        <span class="text-muted">Start Date: {{ $course->start_date }}</span>
<br>
        @if($course->course_image)
                <img src="/uploads/{{ $course->course_image }}" style="width: 100%" alt="No Image Available">
                @endif

        <hr>
<div class="panel panel-default panel-body">
       @foreach ($course->publishedLessons as $lesson)
         
          

    <h4> {{ $loop->iteration }}. {{ $lesson->title }}</h4>
    <div>{!!   $lesson->short_text !!}</div>
  <a href="{{ route('lessons.show', [$lesson->course_id, $lesson->slug]) }}" class="btn btn-primary">Learn </a>
        @endforeach
 
</div>
     
    </div>
    <div class="col-md-4">
        <div class="panel panel-primary">
                <div class="panel-heading"><h2 class="panel-title">
                    Rating: {{ $course->rating }} / 5
                </h2></div>
            <div class="panel-body">
                
                <br>
                <br>
                <form action="{{ route('courses.rating', [$course->id]) }}" method="post">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label> Rate the course: </label>
                        <select name="rating" class="form-control">
                            <option value="1">1 - Awful</option>
                            <option value="2">2 - Not too good</option>
                            <option value="3">3 - Average</option>
                            <option value="4" selected>4 - Quite good</option>
                            <option value="5">5 - Awesome!</option>
                        </select>
                    </div>
                    <input type="submit" value="Rate" class="btn btn-primary btn-block"/>
                </form>
            </div>
        </div>
    </div>
    <hr/>


@endsection